# Python Example

A sample of a python microservice block for [kintohub](http://kintohub.com)


It is using python `3.6.4`

# First time setup
* `pip install -r requirements.txt`

# IMPORTANT
  this example project is built using using docker file from this repo which
  may contain version of language different from the one you've set in Kintohub's UI

  If you want to use the version you've set in Kintohub's UI -> just remove the Dockerfile

# Run
- `FLASK_APP=hello.py flask run` runs on port `5000`
- `FLASK_APP=hello.py flask run --port=80 --host=0.0.0.0` runs on port 80 and the server is accessable by external calls

